import numpy
import os, tempfile
import sys
import signal

def get_partitions (num_entries, num_jobs):

    assert(num_entries >= num_jobs)
    base_partition_size = num_entries // num_jobs
    overhang = num_entries % num_jobs

    partition_sizes = [base_partition_size]*num_jobs

    for i in range(overhang):
        partition_sizes[i] += 1

    assert(sum(partition_sizes) == num_entries)


    partitions = []

    pos = 0
    for i in range(len(partition_sizes)):
        partitions.append((pos, pos + partition_sizes[i])) #start, end (end is exclusive)
        pos += partition_sizes[i]

    return partitions


def sigint_handler_main (signal_number, frame):

    print("Received SIGINT, stopping all worker processes")
    for cpid in cpids:
        os.kill(cpid, signal.SIGINT)
    sys.exit(0)


def sigint_handler_worker (signal_number, frame):

    sys.exit(0)


def map (func, array, num_jobs, outfilename, args=tuple(), kwargs=dict()):

    testresult = func(array[0])
    map_with_dtype(func, array, num_jobs, outfilename, result_dtype=testresult.dtype, result_shape=testresult.shape, args=args, kwargs=kwargs)
    

def map_with_dtype (func, array, num_jobs, outfilename, result_dtype, result_shape, args=tuple(), kwargs=dict()):

    print("parallel-mapper: preparing output memory...")

    shmfilename = outfilename

    shmfile = open(shmfilename, "w+")

    shm_shape = (array.shape[0],) + result_shape
    shm_dtype = result_dtype
    shm_pitch = result_dtype.itemsize * numpy.prod(result_shape)

    testarray = numpy.empty(dtype=shm_dtype, shape=shm_shape)
    properties = numpy.lib.format.header_data_from_array_1_0(testarray)

    numpy.lib.format.write_array_header_1_0(shmfile, properties)
    offset = shmfile.tell()

    data_size = testarray.nbytes
    shmfile.truncate(offset + data_size)
    shmfile.close()


    ##write first result
    #array1 = numpy.memmap(shmfilename, dtype=testresult.dtype, shape=testresult.shape, offset=offset)
    #array1[:] = testresult
    #del(array1)

    print("parallel-mapper: preparing input memory...")

    infile, infilename = tempfile.mkstemp()
    os.close(infile)

    inarray = numpy.memmap(infilename, dtype=array.dtype, shape=array.shape)
    inarray[:] = array[:]
    del(inarray)

    num_entries = array.shape[0]
    num_jobs = min(num_jobs, num_entries)
    partitions = get_partitions(num_entries, num_jobs)

    print("running.")

    cpids = []
    for i in range(num_jobs):

        cpid = os.fork()

        if cpid == 0:
            wrapper(func=func,
                    shmfilename = shmfilename,
                    shm_dtype   = shm_dtype,
                    shm_shape   = shm_shape,
                    shm_pitch   = shm_pitch,
                    shm_offset  = offset,
                    infilename  = infilename,
                    in_dtype    = array.dtype,
                    in_shape    = array.shape,
                    in_pitch    = array[0].nbytes,
                    start       = partitions[i][0],
                    end         = partitions[i][1],
                    func_args   = args,
                    func_kwargs = kwargs,)
            sys.exit(0)

        else:
            cpids.append(cpid)
        
    signal.signal(signal.SIGINT, sigint_handler_main)

    done = [False]*num_jobs
    while False in done:
        pid, status = os.wait()
        index = cpids.index(pid)
        done[index] = True
        print("Process {} ({}) done".format(index, pid))

    os.remove(infilename)
        


def wrapper (func, shmfilename, shm_dtype, shm_shape, shm_pitch, shm_offset, infilename, in_dtype, in_shape, in_pitch, start, end, func_args, func_kwargs):

    signal.signal(signal.SIGINT, sigint_handler_worker)

    pos = start
    while pos < end:
        chunk_size = min(4096, end-pos)

        in_chunk = numpy.memmap(infilename, offset=in_pitch*pos, dtype=in_dtype, shape=(chunk_size,) + in_shape[1:])
        out_chunk = numpy.memmap(shmfilename, offset=shm_pitch*pos + shm_offset, dtype=shm_dtype, shape=(chunk_size,) + shm_shape[1:])

        for i in range(len(in_chunk)):

            print("{}".format(pos+i))

            try:
                out_chunk[i] = func(in_chunk[i], *func_args, **func_kwargs)
            except:
                pass

        pos += chunk_size


def square(x):
    return x**2

if __name__ == "__main__":

    map(lambda x: x**2, numpy.linspace(0, 10, 1000000), num_jobs=4, outfilename="test.npy")
